package load.balancing.worker.client

import load.balancing.worker.application.task.command.TaskCommand
import load.balancing.worker.application.task.port.TaskCommandPort
import load.balancing.worker.application.task.response.TaskResponse
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/task")
class TaskCommandController(private val taskCommandPort: TaskCommandPort) {

    @PostMapping("/factorial")
    fun calculateFactorial(@RequestBody command: TaskCommand): TaskResponse {
        return taskCommandPort.calculateFactorial(command)
    }

}