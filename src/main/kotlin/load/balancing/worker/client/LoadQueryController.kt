package load.balancing.worker.client

import load.balancing.worker.application.load.port.LoadQueryPort
import load.balancing.worker.application.load.projection.WorkerLoadData
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/load")
class LoadQueryController(private val loadQueryPort: LoadQueryPort) {

    @GetMapping
    fun getWorkerLoad(): WorkerLoadData {
        return loadQueryPort.getWorkerLoad()
    }

}