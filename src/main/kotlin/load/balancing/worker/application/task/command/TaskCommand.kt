package load.balancing.worker.application.task.command

import java.util.*

data class TaskCommand(
    val uuid: UUID,
    val taskCount: Int
)