package load.balancing.worker.application.task

import load.balancing.worker.application.task.command.TaskCommand
import load.balancing.worker.application.task.port.TaskCommandPort
import load.balancing.worker.application.task.response.TaskResponse
import org.springframework.stereotype.Service

@Service
internal class TaskCommandAdapter : TaskCommandPort {

  override fun calculateFactorial(command: TaskCommand): TaskResponse =
      executeTimedTask(FactorialTask(
          command.uuid,
          command.taskCount
      ))

  private fun executeTimedTask(task: Task): TaskResponse {
    val timedTask = TimedTask(task)
    timedTask.execute()
    return TaskResponse(
        timedTask.duration
    )
  }

}