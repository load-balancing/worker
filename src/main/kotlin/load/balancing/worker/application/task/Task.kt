package load.balancing.worker.application.task

import java.util.*

interface Task {
    val uuid: UUID
    fun execute()
}