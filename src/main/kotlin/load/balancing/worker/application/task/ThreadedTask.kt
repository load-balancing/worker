package load.balancing.worker.application.task

import java.util.stream.IntStream

class ThreadedTask(private val task: Task, private val numberOfTasks: Int) {

    fun execute() {
        IntStream.range(0, numberOfTasks)
                .boxed()
                .parallel()
                .forEach { task.execute() }
    }

}