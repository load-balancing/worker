package load.balancing.worker.application.task

import com.google.common.base.Stopwatch
import mu.KotlinLogging
import java.time.Duration

internal class TimedTask(private val task: Task) {

  private val stopwatch = Stopwatch.createUnstarted();
  val duration: Duration
    get() = stopwatch.elapsed()

  fun execute() {
    logger.info { "Starting task ${task.uuid}" }
    stopwatch.start()
    task.execute()
    stopwatch.stop()
    logger.info { "Ending task ${task.uuid}, duration: ${stopwatch.elapsed()}" }
  }

  companion object {
    private val logger = KotlinLogging.logger {}
  }

}