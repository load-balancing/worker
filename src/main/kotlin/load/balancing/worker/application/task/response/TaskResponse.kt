package load.balancing.worker.application.task.response

import java.time.Duration

data class TaskResponse(
    val duration: Duration
)