package load.balancing.worker.application.task

import java.math.BigInteger
import java.util.*

internal class FactorialTask(override val uuid: UUID, private val number: Int) : Task {

    override fun execute() {
        var result: BigInteger = BigInteger.ONE

        (2..20000).forEach { factor ->
            result = result.multiply(BigInteger.valueOf(factor.toLong()))
        }

    }

}