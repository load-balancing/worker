package load.balancing.worker.application.task.port

import load.balancing.worker.application.task.command.TaskCommand
import load.balancing.worker.application.task.response.TaskResponse

interface TaskCommandPort {
    fun calculateFactorial(command: TaskCommand): TaskResponse
}