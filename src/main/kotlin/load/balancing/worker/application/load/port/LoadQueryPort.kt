package load.balancing.worker.application.load.port

import load.balancing.worker.application.load.projection.WorkerLoadData

interface LoadQueryPort {
    fun getWorkerLoad() : WorkerLoadData
}