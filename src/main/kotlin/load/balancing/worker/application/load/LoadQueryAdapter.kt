package load.balancing.worker.application.load

import load.balancing.worker.application.load.port.LoadQueryPort
import load.balancing.worker.application.load.projection.WorkerLoadData
import load.balancing.worker.infrastructure.instance.port.InstanceInterfacePort
import load.balancing.worker.infrastructure.metrics.port.MetricsInterfacePort
import org.springframework.stereotype.Service

@Service
internal class LoadQueryAdapter(private val metricsInterfacePort: MetricsInterfacePort,
                                private val instanceInterfacePort: InstanceInterfacePort) : LoadQueryPort {

    override fun getWorkerLoad(): WorkerLoadData {
        return WorkerLoad(
                instanceId = instanceInterfacePort.getInstanceId(),
                cpuLoad = metricsInterfacePort.getCpuLoad()
        )
    }

}