package load.balancing.worker.application.load

import load.balancing.worker.application.load.projection.WorkerLoadData

internal class WorkerLoad(override val instanceId: String,
                          override val cpuLoad: Double) : WorkerLoadData