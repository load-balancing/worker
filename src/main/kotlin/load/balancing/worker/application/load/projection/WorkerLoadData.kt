package load.balancing.worker.application.load.projection

interface WorkerLoadData {
    val instanceId: String
    val cpuLoad: Double
}