package load.balancing.worker.infrastructure.instance.port

interface InstanceInterfacePort {
    fun getInstanceId() : String
}