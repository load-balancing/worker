package load.balancing.worker.infrastructure.instance

import load.balancing.worker.infrastructure.instance.port.InstanceInterfacePort
import org.springframework.cloud.netflix.eureka.EurekaInstanceConfigBean
import org.springframework.stereotype.Service

@Service
internal class InstanceInterfaceAdapter(private val eurekaInstanceConfigBean: EurekaInstanceConfigBean) : InstanceInterfacePort {

    override fun getInstanceId(): String {
        return eurekaInstanceConfigBean.instanceId
    }

}
