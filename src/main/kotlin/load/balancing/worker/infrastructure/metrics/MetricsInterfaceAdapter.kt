package load.balancing.worker.infrastructure.metrics

import load.balancing.worker.infrastructure.metrics.port.MetricsInterfacePort
import org.springframework.boot.actuate.metrics.MetricsEndpoint
import org.springframework.stereotype.Service

@Service
class MetricsInterfaceAdapter(private val metricsEndpoint: MetricsEndpoint) : MetricsInterfacePort {

    override fun getCpuLoad(): Double {
        return metricsEndpoint.metric("system.cpu.usage", null).measurements
                .map { it.value }
                .first()
    }

}