package load.balancing.worker.infrastructure.metrics.port

interface MetricsInterfacePort {
    fun getCpuLoad() : Double
}